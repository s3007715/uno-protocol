# UNO Protocol for inter-application communication

## Commands
| **Command**     | **Sender** | **Receiver**                      | **Arguments**                                                                                                                                                                                                  | **Description**                                                                                                                                                                            |
|-----------------|------------|-----------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `hi`            | Client     | Server                            | `displayName` - the name chosen by the client `functionalities` - the functionalities that are enabled on the client (e.g. chat)                                                                               | Makes the initial connection to the server.                                                                                                                                                |
| `connected`     | Server     | Client                            |                                                                                                                                                                                                                | Confirms that the initial connection has been made.                                                                                                                                        |
| `joinGame`      | Client     | Server                            | `gameSize: int` - size of the requested game (between 1 and 9)                                                                                                                                                 | Requests a game of the given gameSize. Puts the player in a queue for that game size.                                                                                                      |
| `queueJoined`   | Server     | All clients in queue              | `displayName: String` - the name of the player that joined the queue `playerNames: String[]` - the names of all players in the queue                                                                           | Confirms that the player has been put into the queue. Sends the displayName of the user that has joined the queue. Also sends the playerNames of the players in the queue for convenience. |
| `queueLeft`     | Server     | All clients in the queue          | `displayName: String` - the user that joined the queue `playerNames: String[]` - the names of all players in the queue (for convenience)                                                                       | Broadcasts that a client has left the queue (client closed the TCP connection).                                                                                                            |
| `gameStarted`   | Server     | All clients in queue              |                                                                                                                                                                                                                | Broadcasts that a new game has started (the lobby was filled).                                                                                                                             |
| `roundStarted`  | Server     | All clients in game, individually | `initialCards: String` - the 7 initial cards for the client                                                                                                                                                    | Broadcasts that a new round has started. Sends the initial 7 cards to each player individually.                                                                                            |
| `playCard`      | Client     | Server                            | `card: CardObject` - the card that the player wants to play                                                                                                                                                    | Requests the server to play a card on behalf of the client. (Server should validate it!)                                                                                                   |
| `nextTurn`      | Server     | All clients in game               | `playedCard: CardObject` - the card played by the previous player `displayName: String` - the name of the next player `prevPlayerCardCount: int` - the amount of cards that the previous player still has left | Broadcasts that a card has been played and that it is now ```displayName```'s turn.                                                                                                        |
| `drawCard`      | Client     | Server                            |                                                                                                                                                                                                                | Requests the server to draw a card on behalf of the client.                                                                                                                                |
| `issueCard`     | Server     | Client                            | `card: CardObject[]` - the cards that are given to the client                                                                                                                                                  | Gives one or more cards to a specific client.                                                                                                                                              |
| `roundFinished` | Server     | All clients in game               | `displayName: String` - the name of the winning player `winnerScore: int` - the score of the winning player                                                                                                    | Broadcasts that the current round has finished.                                                                                                                                            |
| `gameFinished`  | Server     | All clients in game               | `scoreboard: ScoreboardObject[]` - a key-value set of each player with their correspondings core                                                                                                               | Broadcasts that the game has finished.                                                                                                                                                     |
| `error`         | Server     | Client                            | `code` `description` See the table of error codes.                                                                                                                                                             | Informs the client that an error has occurred.                                                                                                                                             |

## Error codes
| **Code**                  | **Description**                                                       |
|---------------------------|-----------------------------------------------------------------------|
| ```invalid_move```        | The move is invalid.                                                  |
| ```name_taken```          | This name has already been taken by another player in the same queue. |
| ```invalid_name```        | Invalid name, character(s) are not part of UTF-8.                     |
| ```invalid_command```     | The command is invalid.                                               |
| ```invalid_argument```    | The argument is invalid.                                              |
| ```player_disconnected``` | A player has disconnected from the game.                              |

## Card Modeling

Each card object (`CardObject`) that is sent over the network, is composed of one `color` and one `value`.

| **Color**    | **Encoding** |
|--------------|--------------|
| Red          | ```R```      |
| Green        | ```G```      |
| Blue         | ```B```      |
| Yellow       | ```Y```      |
| Wild (black) | ```?```      |

| **Card Value**   | **Encoding** |
|------------------|--------------|
| 0-9              | ```0-9```    |
| Skip             | ```S```      |
| Draw             | ```D```      |
| Wildcard         | ```W```      |
| Wildcard + drw 4 | ```F```      |
| Reverse card     | ```R```      |

